#pragma once
#include "Token.h"
#include <set>
#include <vector>
using namespace std;
struct Symbol {
	enum Type
	{
		INT,
		CHAR,
		VOID,
		INIT
	};
	Token token;
	Type type;
	bool operator < (const Symbol &b) const {
		return (this->token) < (b.token);
	}
};
struct SymbolTable
{
	std::set<Symbol> symbolSet;
	enum Type
	{
		RE_FUN,
		VOID_FUN
	};
	bool has(Token& checkToken);

	void insert(vector<Token>::iterator& iter, SymbolTable::Type symType) {
		Symbol tmpSym;
		switch (symType)
		{
		case SymbolTable::RE_FUN:
			if (iter->getType() == Token::INTTK)
				tmpSym.type = Symbol::INT;
			else
				tmpSym.type = Symbol::CHAR;
			tmpSym.token = *(iter + 1);
			break;
		case SymbolTable::VOID_FUN:
			tmpSym.type = Symbol::VOID;
			tmpSym.token = *(iter + 1);
			break;
		default:
			break;
		}
		symbolSet.insert(tmpSym);
	}
};

