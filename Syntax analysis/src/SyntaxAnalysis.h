#pragma once
#include <vector>
#include "Token.h"
using namespace std;
//////////////////////////����ԭ��
int SynMain(vector<Token> vector);
int Syn_PLUS(vector<Token>::iterator& iter, bool show);
int Syn_MULT(vector<Token>::iterator& iter, bool show);
int Syn_CMP(vector<Token>::iterator& iter, bool show);
int Syn_STRCON(vector<Token>::iterator& iter, bool show);
int Syn_UNSIGNED_INTCON(vector<Token>::iterator& iter, bool show);
int Syn_INT(vector<Token>::iterator& iter, bool show);
int Syn_IDENFR(vector<Token>::iterator& iter, bool show);
int Syn_DEC_HEADER(vector<Token>::iterator& iter, bool show);
int Syn_TYPE_IDEN(vector<Token>::iterator& iter, bool show);
int Syn_VAR_DEF(vector<Token>::iterator& iter, bool show);
int Syn_VAR_DEC(vector<Token>::iterator& iter, bool show);
int Syn_PARA_LIST(vector<Token>::iterator& iter, bool show);
int Syn_EXP(vector<Token>::iterator& iter, bool show);
int Syn_TERM(vector<Token>::iterator& iter, bool show);
int Syn_FACTOR(vector<Token>::iterator& iter, bool show);
int Syn_RE_FUNC_CALL(vector<Token>::iterator& iter, bool show);
int Syn_VOID_FUNC_CALL(vector<Token>::iterator& iter, bool show);
int Syn_CONST_DEF(vector<Token>::iterator& iter, bool show);
int Syn_COMPLEX_STATE(vector<Token>::iterator& iter, bool show);
int Syn_STATE_LIST(vector<Token>::iterator& iter, bool show);
int Syn_STATEMENT(vector<Token>::iterator& iter, bool show);
//////////////////////////////////