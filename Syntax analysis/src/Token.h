#pragma once
#include <iostream>
#include <string>
struct Token
{

public:
	enum TokenType
	{
		INIT,
		IDENFR,
		INTCON,
		CHARCON,
		STRCON,
		CONSTTK,
		INTTK,
		CHARTK,
		VOIDTK,
		MAINTK,
		IFTK, 
		ELSETK,
		DOTK,
		WHILETK,
		FORTK,
		SCANFTK,
		PRINTFTK,
		RETURNTK,
		PLUS,
		MINU,
		MULT,
		DIV,
		LSS,
		LEQ,
		GRE,
		GEQ,
		EQL,
		NEQ,
		ASSIGN,
		SEMICN,
		COMMA,
		LPARENT,
		RPARENT,
		LBRACK,
		RBRACK,
		LBRACE,
		RBRACE,
		NOT,
		ERROR
	};
	
	std::string tokenString;
	TokenType type;
	bool tokenFinish;
public:
	Token();
	int init(char*& str);
	const void show();
	const TokenType getType();
	bool operator < (const Token& b) const {
		return this->tokenString< b.tokenString;
	}
};

