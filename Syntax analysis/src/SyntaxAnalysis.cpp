﻿#include <iostream>
#include <vector>
#include "Token.h"
#include "SyntaxAnalysis.h"
#include "SymbolTable.h"

using namespace std;
vector<Token>* tokenVectorPointer;
#define tokenVector (*tokenVectorPointer)
SymbolTable reFunTable, voidFunTable;

void ShowAllToken() {
	for (int i = 0; i < tokenVector.size(); i++) {
		tokenVector[i].show();
	}
}
inline int Syn_TK(vector<Token>::iterator& iter, Token::TokenType type) {
	return iter->type == type;
}
inline int Syn_TK(vector<Token>::iterator& iter,Token::TokenType type, bool show) {
	if (iter->type != type)
		return 0;
	if (show) {

		iter->show();
		if (type == Token::INTCON)
			cout << "<无符号整数>\n";
	}
	iter++;
	return 1;
}

//<加法运算符>::= +｜-
int Syn_PLUS(vector<Token>::iterator& iter,bool show=0) {
	if (Syn_TK(iter, Token::PLUS, show))
		return 1;
	if (Syn_TK(iter, Token::MINU, show))
		return 1;
	return 0;
}
//<乘法运算符> ::= *｜/
int Syn_MULT(vector<Token>::iterator& iter, bool show=0) {
	if (Syn_TK(iter, Token::MULT, show))
		return 1;
	if (Syn_TK(iter, Token::DIV, show))
		return 1;
	return 0;
}

//<关系运算符> ::=  <｜<=｜>｜>=｜!=｜==
int Syn_CMP(vector<Token>::iterator& iter, bool show=0) {
	Token& token = *iter;
	auto type = token.getType();
	if (type == Token::LSS || type == Token::LEQ || type == Token::GRE || type == Token::GEQ || type == Token::NEQ || type == Token::EQL) {
		if (show) 
			token.show();
		iter++;
		return 1;
	}
	return 0;
}
//<字符串>  ::=  "｛十进制编码为32,33,35-126的ASCII字符｝"
int Syn_STRCON(vector<Token>::iterator& iter, bool show=0) {
	int ans = Syn_TK(iter, Token::STRCON, show);
	if(show && ans)
		cout << "<字符串>\n";
	return ans;
}
//<无符号整数> ::= <非零数字>｛<数字>｝| 0
int Syn_UNSIGNED_INTCON(vector<Token>::iterator& iter, bool show=0) {
	int ans = Syn_TK(iter, Token::INTCON);
	if (show && ans)
		cout << "<无符号整数>\n";
	return ans;
}
//<整数>       ::= ［＋｜－］<无符号整数>
int Syn_INT(vector<Token>::iterator& iter, bool show = 0) {
	auto iter2 = iter;
	int flag = 0;
	Syn_PLUS(iter2);
	if (!Syn_TK(iter2,Token::INTCON)) {
		return 0;
	}
	Syn_PLUS(iter,show);
	Syn_TK(iter,Token::INTCON,show);
	if (show)
		cout << "<整数>\n";
	return 1;
}
//<标识符>   ::=  <字母>｛<字母>｜<数字>｝
inline int Syn_IDENFR(vector<Token>::iterator& iter, bool show = 0) {
	return Syn_TK(iter, Token::IDENFR, show);
}
inline int Syn_CHARCON(vector<Token>::iterator& iter, bool show = 0) {
	return Syn_TK(iter, Token::CHARCON, show);
}
inline int Syn_CONSTTK(vector<Token>::iterator& iter, bool show = 0) {
	return Syn_TK(iter, Token::CONSTTK, show);
}

inline int Syn_INT_OR_CHARTK(vector<Token>::iterator& iter, bool show = 0) {
	if (Syn_TK(iter, Token::INTTK, show))
		return 1;
	if (Syn_TK(iter, Token::CHARTK, show))
		return 1;
	return 0;
}
//<声明头部>  ::=  int<标识符>|char<标识符>
int Syn_DEC_HEADER(vector<Token>::iterator& iter, bool show = 0) {
	auto iter2 = iter;
	if (!Syn_INT_OR_CHARTK(iter2)) {
		return 0;
	}
	if (!Syn_IDENFR(iter2)) {
		return 0;
	}
	Syn_INT_OR_CHARTK(iter, show);
	Syn_IDENFR(iter, show);
	if (show)
		cout << "<声明头部>\n";
	return 1;
}
//<类型标识符>     ::=  int | char
int Syn_TYPE_IDEN(vector<Token>::iterator& iter, bool show = 0){
	auto type = (*iter).getType();
	if (type == Token::INTTK || type == Token::CHARTK) {
		if (show) 
			(*iter).show();
		iter++;
		return 1;
	}
	return 0;
}
inline int Syn_LBRACK_UINT_RBRACK(vector<Token>::iterator& iter, bool show = 0) {
	auto iter2 = iter;
	/*if (!Syn_TK(iter2, Token::LBRACK))
		return 0;
	if (!Syn_INT(iter2))
		return 0;
	if (!Syn_TK(iter2, Token::RBRACK))
		return 0;*/
	if(!Syn_TK(iter, Token::LBRACK, show))
		return 0;
	Syn_INT(iter,show);
	Syn_TK(iter, Token::RBRACK, show);
	return 1;
}
//<变量定义> ::= <类型标识符>(<标识符>|<标识符>'['<无符号整数>']'){,(<标识符>|<标识符>'['<无符号整数>']' )}

                 //<无符号整数>表示数组元素的个数，其值需大于0
//TODO:添加对于无符号整数的判断
//TODO:添加对于vector越界的判定
int Syn_VAR_DEF(vector<Token>::iterator& iter, bool show = 0) {
	auto iter2 = iter;
	int ans = 0;
	if (!Syn_TYPE_IDEN(iter2))
		return 0;
	if (!Syn_IDENFR(iter2))
		return 0;
	Syn_LBRACK_UINT_RBRACK(iter2);
	ans = 1;
	Syn_TYPE_IDEN(iter, show);
	Syn_IDENFR(iter, show);
	Syn_LBRACK_UINT_RBRACK(iter, show);
	while (1) {
		if (!Syn_TK(iter2, Token::COMMA))
			break;
		if (!Syn_IDENFR(iter2))
			break;
		Syn_LBRACK_UINT_RBRACK(iter2);

		Syn_TK(iter, Token::COMMA,show);
		Syn_IDENFR(iter, show);
		Syn_LBRACK_UINT_RBRACK(iter, show);
	}
	if (ans && show)
		cout << "<变量定义>\n";
	return ans;
}

//!<变量说明> ::= <变量定义>;{<变量定义>;}
int Syn_VAR_DEC(vector<Token>::iterator& iter, bool show = 0) {
	auto iter2 = iter;
	int ans = 0;
	int x;
	while (1) {
		if (!Syn_VAR_DEF(iter2))
			break;
		if (!Syn_TK(iter2, Token::SEMICN))
			break;
		ans = 1;
		Syn_VAR_DEF(iter, show);
		Syn_TK(iter, Token::SEMICN,show);
	}
	if (ans && show)
		cout << "<变量说明>\n";
	return ans;
}
//<参数表>   ::=  <类型标识符><标识符>{,<类型标识符><标识符>}| <空>
int Syn_PARA_LIST(vector<Token>::iterator& iter, bool show = 0) {
	//永远匹配成功
	auto iter2 = iter;
	if (!Syn_TYPE_IDEN(iter2))
		return 1;
	if (!Syn_IDENFR(iter2))
		return 1;
	Syn_TYPE_IDEN(iter, show);
	Syn_IDENFR(iter, show);
	while (1) {
		if (!Syn_TK(iter2, Token::COMMA))
			break;
		if (!Syn_TYPE_IDEN(iter2))
			break;
		if (!Syn_IDENFR(iter2))
			break;
		Syn_TK(iter,Token::COMMA, show);
		Syn_TYPE_IDEN(iter, show);
		Syn_IDENFR(iter, show);
	}
	if (show)
		cout << "<参数表>\n";
	return 1;
}
//<表达式>   ::= ［＋｜－］<项>{<加法运算符><项>}   //[+|-]只作用于第一个<项>
int Syn_EXP(vector<Token>::iterator& iter, bool show = 0) {
	auto iter2 = iter;
	Syn_PLUS(iter2);
	if (!Syn_TERM(iter2,0))
		return 0;
	Syn_PLUS(iter, show);
	Syn_TERM(iter, show);
	while (1) {
		if (!Syn_PLUS(iter2))
			break;
		if (!Syn_TERM(iter2,0))
			break;
		Syn_PLUS(iter, show);
		Syn_TERM(iter, show);
	}
	if (show)
		cout << "<表达式>\n";
	return 1;
}
//<项>    ::= <因子>{<乘法运算符><因子>}
int Syn_TERM(vector<Token>::iterator& iter, bool show = 0) {
	auto iter2 = iter;
	if (!Syn_FACTOR(iter2,0))
		return 0;
	Syn_FACTOR(iter, show);
	while (1) {
		if (!Syn_MULT(iter2))
			break;
		if (!Syn_FACTOR(iter2,0))
			break;
		Syn_MULT(iter, show);
		Syn_FACTOR(iter, show);
	}
	if (show)
		cout << "<项>\n";
	return 1;
}
//'['<表达式>']'
inline int Syn_LBRACK_EXP_RBRACK(vector<Token>::iterator& iter, bool show = 0) {
	auto iter2 = iter;
	if (!Syn_TK(iter2, Token::LBRACK))
		return 0;
	if (!Syn_EXP(iter2))
		return 0;
	if (!Syn_TK(iter2, Token::RBRACK))
		return 0;
	Syn_TK(iter, Token::LBRACK);
	Syn_EXP(iter);
	Syn_TK(iter, Token::RBRACK);
	return 1;
}
//'('<表达式>')'
inline int Syn_LPARENT_EXP_RPARENT(vector<Token>::iterator& iter, bool show = 0) {
	auto iter2 = iter;
	if (!Syn_TK(iter2, Token::LPARENT))
		return 0;
	if (!Syn_EXP(iter2))
		return 0;
	if (!Syn_TK(iter2, Token::RPARENT))
		return 0;
	Syn_TK(iter, Token::LPARENT,show);
	Syn_EXP(iter,show);
	Syn_TK(iter, Token::RPARENT,show);
	return 1;
}
//<字符>   ::=  '<加法运算符>'｜'<乘法运算符>'｜'<字母>'｜'<数字>'
int Syn_CHAR(vector<Token>::iterator& iter, bool show = 0) {
	return Syn_TK(iter, Token::CHARCON, show);
}

//<因子>   ::= <标识符>｜<标识符>'['<表达式>']'|'('<表达式>')'｜<整数>|<字符>｜<有返回值函数调用语句>   
int Syn_FACTOR(vector<Token>::iterator& iter, bool show = 0){
	auto iter2 = iter;
	bool flag = 0;
	if (Syn_RE_FUNC_CALL(iter2, 0)) {
		Syn_RE_FUNC_CALL(iter, show);
		flag = 1;
	}else if (Syn_IDENFR(iter2)) {
		Syn_IDENFR(iter, show);
		Syn_LBRACK_EXP_RBRACK(iter, show);
		flag = 1;
	}
	else if (Syn_LPARENT_EXP_RPARENT(iter2)) {
		Syn_LPARENT_EXP_RPARENT(iter, show);
		flag = 1;
	}
	else if (Syn_INT(iter2)) {
		Syn_INT(iter, show);
		flag = 1;
	}
	else if (Syn_CHAR(iter2)) {
		Syn_CHAR(iter, show);
		flag = 1;
	}
	if (flag && show)
		cout << "<因子>\n";
	return flag;
}
//<值参数表>  ::= <表达式>{,<表达式>}｜<空>
int Syn_VAL_PARA_LIST(vector<Token>::iterator& iter, bool show = 0) {
	auto iter2 = iter;
	if (!Syn_EXP(iter2))
		return 0;
		Syn_EXP(iter, show);
		while (1) {
			if (!Syn_TK(iter2, Token::COMMA))
				break;
			if (!Syn_EXP(iter2))
				break;
			Syn_TK(iter, Token::COMMA, show);
			Syn_EXP(iter, show);
		}

	
	if (show)
		cout << "<值参数表>\n";
	return 1;
}
//<有返回值函数调用语句>::= <标识符>'('<值参数表>')'
int Syn_RE_FUNC_CALL(vector<Token>::iterator& iter, bool show = 0) {
	auto iter2 = iter;
	if (!reFunTable.has(*iter))
		return 0;
	if (!Syn_IDENFR(iter2))
		return 0;
	if (!Syn_TK(iter2, Token::LPARENT))
		return 0;
	if (Syn_TK(iter2, Token::RPARENT)) {
		Syn_IDENFR(iter, show);
		Syn_TK(iter, Token::LPARENT, show);
		Syn_TK(iter, Token::RPARENT, show);
	}
	else {
		if (!Syn_VAL_PARA_LIST(iter2))
			return 0;
		if (!Syn_TK(iter2, Token::RPARENT))
			return 0;
		Syn_IDENFR(iter, show);
		Syn_TK(iter, Token::LPARENT, show);
		Syn_VAL_PARA_LIST(iter, show);
		Syn_TK(iter, Token::RPARENT, show);
	}
	if (show)
		cout << "<有返回值函数调用语句>\n";
	return 1;
}
//<无返回值函数调用语句>:: = <标识符>'('<值参数表>')'
int Syn_VOID_FUNC_CALL(vector<Token>::iterator& iter, bool show = 0) {
	auto iter2 = iter;
	if (!voidFunTable.has(*iter))
		return 0;
	if (!Syn_IDENFR(iter2))
		return 0;
	if (!Syn_TK(iter2, Token::LPARENT))
		return 0;
	if (Syn_TK(iter2, Token::RPARENT)) {
		Syn_IDENFR(iter, show);
		Syn_TK(iter, Token::LPARENT, show);
		Syn_VAL_PARA_LIST(iter, show);
		Syn_TK(iter, Token::RPARENT, show);
	}
	else {
		if (!Syn_VAL_PARA_LIST(iter2))
			return 0;
		if (!Syn_TK(iter2, Token::RPARENT))
			return 0;
		Syn_IDENFR(iter, show);
		Syn_TK(iter, Token::LPARENT, show);
		Syn_VAL_PARA_LIST(iter, show);
		Syn_TK(iter, Token::RPARENT, show);
	}
	if (show)
		cout << "<无返回值函数调用语句>\n";
	return 1;
}
//<主函数>   ::= void main‘(’‘)’ ‘{’<复合语句>‘}’
int Syn_MAIN_FUNC(vector<Token>::iterator& iter, bool show = 0) {
	auto iter2 = iter;
	if (!Syn_TK(iter2, Token::VOIDTK))
		return 0;
	if (!Syn_TK(iter2, Token::MAINTK))
		return 0;
	if (!Syn_TK(iter2, Token::LPARENT))
		return 0;
	if (!Syn_TK(iter2, Token::RPARENT))
		return 0;
	if (!Syn_TK(iter2, Token::LBRACE))
		return 0;
	if (!Syn_COMPLEX_STATE(iter2, 0))
		return 0;
	if (!Syn_TK(iter2, Token::RBRACE))
		return 0;
	Syn_TK(iter, Token::VOIDTK, show);
	Syn_TK(iter, Token::MAINTK, show);
	Syn_TK(iter, Token::LPARENT, show);
	Syn_TK(iter, Token::RPARENT, show);
	Syn_TK(iter, Token::LBRACE, show);
	Syn_COMPLEX_STATE(iter, show);
	Syn_TK(iter, Token::RBRACE, show);
	if (show)
		cout << "<主函数>\n";
	return 1;
}
//<常量说明>::=  const<常量定义>;{ const<常量定义>;}
int Syn_CONST_DEC(vector<Token>::iterator& iter, bool show = 0) {
	auto iter2 = iter;
	int ans = 0;
	while (1) {
		if (!Syn_TK(iter2, Token::CONSTTK))
			break;
		if (!Syn_CONST_DEF(iter2,0))
			break;
		if (!Syn_TK(iter2, Token::SEMICN))
			break;
		ans = 1;
		Syn_TK(iter, Token::CONSTTK, show);
		Syn_CONST_DEF(iter, show);
		Syn_TK(iter, Token::SEMICN, show);
	}
	if (ans && show)
		cout << "<常量说明>\n";
	return ans;
}
//<常量定义>  ::=   int<标识符>＝<整数>{,<标识符>＝<整数>}| char<标识符>＝<字符>{ ,<标识符>＝<字符>}
int Syn_CONST_DEF(vector<Token>::iterator& iter, bool show = 0) {
	auto iter2 = iter;
	int ans = 0;
	if (Syn_TK(iter2, Token::INTTK)) {//int
		if (!Syn_IDENFR(iter2))
			return 0;
		if (!Syn_TK(iter2, Token::ASSIGN))
			return 0;
		if (!Syn_INT(iter2))
			return 0; 
		Syn_TK(iter, Token::INTTK,show);
		Syn_IDENFR(iter, show);
		Syn_TK(iter, Token::ASSIGN,show);
		Syn_INT(iter, show);
		ans = 1;
		while (Syn_TK(iter2,Token::COMMA)) {
			if (!Syn_IDENFR(iter2))
				break;
			if (!Syn_TK(iter2, Token::ASSIGN))
				break;
			if (!Syn_INT(iter2))
				break;
			Syn_TK(iter, Token::COMMA, show);
			Syn_IDENFR(iter, show);
			Syn_TK(iter, Token::ASSIGN, show);
			Syn_INT(iter, show);
		}
	}
	else if (Syn_TK(iter2, Token::CHARTK)) {//char
		if (!Syn_IDENFR(iter2))
			return 0;
		if (!Syn_TK(iter2, Token::ASSIGN))
			return 0;
		if (!Syn_CHAR(iter2))
			return 0;
		Syn_TK(iter, Token::CHARTK, show);
		Syn_IDENFR(iter, show);
		Syn_TK(iter, Token::ASSIGN, show);
		Syn_CHAR(iter, show);
		ans = 1;
		while (Syn_TK(iter2, Token::COMMA)) {
			if (!Syn_IDENFR(iter2))
				break;
			if (!Syn_TK(iter2, Token::ASSIGN))
				break;
			if (!Syn_CHAR(iter2))
				break;
			Syn_TK(iter, Token::COMMA, show);
			Syn_IDENFR(iter, show);
			Syn_TK(iter, Token::ASSIGN, show);
			Syn_CHAR(iter, show);
		}
	}
	if (ans && show)
		cout << "<常量定义>\n";
	return ans;
}

//<复合语句>  ::=  ［<常量说明>］［<变量说明>］<语句列>
int Syn_COMPLEX_STATE(vector<Token>::iterator& iter, bool show = 0) {
	auto iter2 = iter;
	Syn_CONST_DEC(iter2);
	Syn_VAR_DEC(iter2);
	if (!Syn_STATE_LIST(iter2, 0))
		return 0;
	Syn_CONST_DEC(iter, show);
	Syn_VAR_DEC(iter, show);
	Syn_STATE_LIST(iter, show);
	if (show)
		cout << "<复合语句>\n";
	return 1;
}

//<有返回值函数定义> ::=  <声明头部>'('<参数表>')' '{'<复合语句>'}'
int Syn_RE_FUNC_DEC(vector<Token>::iterator& iter, bool show = 0) {
	auto iter2 = iter;
	bool hasPara;
	if (!Syn_DEC_HEADER(iter2))
		return 0;
	if (!Syn_TK(iter2, Token::LPARENT))
		return 0;
	hasPara = iter2->type != Token::RPARENT;
	if (hasPara) {
		if (!Syn_PARA_LIST(iter2))
			return 0;
	}
	if (!Syn_TK(iter2, Token::RPARENT))
		return 0;
	if (!Syn_TK(iter2, Token::LBRACE))
		return 0;

	reFunTable.insert(iter, SymbolTable::RE_FUN);
	//TODO:
	if (!Syn_COMPLEX_STATE(iter2))
		return 0;
	if (!Syn_TK(iter2, Token::RBRACE))
		return 0;
	Syn_DEC_HEADER(iter, show);
	Syn_TK(iter, Token::LPARENT, show);
	if(hasPara)
		Syn_PARA_LIST(iter, show);
	Syn_TK(iter, Token::RPARENT, show);
	Syn_TK(iter, Token::LBRACE, show);
	Syn_COMPLEX_STATE(iter, show);
	Syn_TK(iter, Token::RBRACE, show);
	if (show) {
		cout << "<有返回值函数定义>\n";
	}
	return 1;
}

//<无返回值函数定义> :: = void<标识符>'('<参数表>')''{'<复合语句>'}'
int Syn_VOID_FUNC_DEC(vector<Token>::iterator& iter, bool show = 0) {
	auto iter2 = iter;
	bool hasPara;
	if (!Syn_TK(iter2, Token::VOIDTK))
		return 0;
	if (!Syn_IDENFR(iter2))
		return 0;
	if (!Syn_TK(iter2, Token::LPARENT))
		return 0;
	hasPara = iter2->type != Token::RPARENT;
	if (hasPara) {
		if (!Syn_PARA_LIST(iter2))
			return 0;
	}
	if (!Syn_TK(iter2, Token::RPARENT))
		return 0;
	if (!Syn_TK(iter2, Token::LBRACE))
		return 0;

	voidFunTable.insert(iter, SymbolTable::VOID_FUN);
	if (!Syn_COMPLEX_STATE(iter2))
		return 0;
	if (!Syn_TK(iter2, Token::RBRACE))
		return 0;
	Syn_TK(iter, Token::VOIDTK,show);
	Syn_IDENFR(iter,show);
	Syn_TK(iter, Token::LPARENT, show);
	if(hasPara)
		Syn_PARA_LIST(iter, show);
	Syn_TK(iter, Token::RPARENT, show);
	Syn_TK(iter, Token::LBRACE, show);
	Syn_COMPLEX_STATE(iter, show);
	Syn_TK(iter, Token::RBRACE, show);
	if (show)
		cout << "<无返回值函数定义>\n";
	return 1;
}

//<返回语句>  ::=  return['('<表达式>')']   
int Syn_RETURN_STATE(vector<Token>::iterator& iter, bool show = 0) {
	int ans = Syn_TK(iter, Token::RETURNTK,show);
	if (ans)
		Syn_LPARENT_EXP_RPARENT(iter, show);
	if (ans && show)
		cout << "<返回语句>\n";
	return ans;
}

//<字符串>,<表达式>|<字符串>|<表达式>
inline int Syn_STR_EXP_MIX(vector<Token>::iterator& iter, bool show = 0) {
	if (Syn_EXP(iter, show)) {
		return 1;
	}
	auto iter2 = iter;
	if (!Syn_STRCON(iter2))//第一个不是表达式，并且不是字符串，那么就gg
		return 0;
	if (Syn_TK(iter2, Token::COMMA)) {//判断是否有逗号
		if (!Syn_EXP(iter2))
			return 0;
		Syn_STRCON(iter, show);
		Syn_TK(iter, Token::COMMA, show);
		Syn_EXP(iter, show);
		return 1;
	}
	else {
		Syn_STRCON(iter, show);
		return 1;//只有字符串
	}
}
//<写语句>   ::= printf '(' <字符串>,<表达式>')'| printf '('<字符串>')'| printf '('<表达式>')'
int Syn_PRINTF_STATE(vector<Token>::iterator& iter, bool show = 0) {
	auto iter2 = iter;
	int x, y;
	if (!Syn_TK(iter2, Token::PRINTFTK))
		return 0;
	if (!Syn_TK(iter2, Token::LPARENT))
		return 0;
	if (!Syn_STR_EXP_MIX(iter2))
		return 0;
	if (!Syn_TK(iter2, Token::RPARENT))
		return 0;
	Syn_TK(iter, Token::PRINTFTK , show);
	Syn_TK(iter, Token::LPARENT , show);
	Syn_STR_EXP_MIX(iter , show);
	Syn_TK(iter, Token::RPARENT , show);
	if (show)
		cout << "<写语句>\n";
	return 1;
}
//<读语句>   ::=  scanf '('<标识符>{,<标识符>}')'
int Syn_SCANF_STATE(vector<Token>::iterator& iter, bool show = 0) {
	auto iter2 = iter;
	if (!Syn_TK(iter2, Token::SCANFTK))
		return 0;
	if (!Syn_TK(iter2, Token::LPARENT))
		return 0;
	if (!Syn_IDENFR(iter2))
		return 0;
	while (Syn_TK(iter2, Token::COMMA)) {
		if (!Syn_IDENFR(iter2))
			return 0;
	}
	if (!Syn_TK(iter2, Token::RPARENT))
		return 0;
	Syn_TK(iter, Token::SCANFTK, show);
	Syn_TK(iter, Token::LPARENT, show);
	Syn_IDENFR(iter, show);
	while (Syn_TK(iter, Token::COMMA,show)) {
		Syn_IDENFR(iter, show);
	}
	Syn_TK(iter, Token::RPARENT, show);
	if (show)
		cout << "<读语句>\n";
	return 1;
}
//<语句列>  ::= ｛<语句>｝
int Syn_STATE_LIST(vector<Token>::iterator& iter, bool show = 0) {
	while (Syn_STATEMENT(iter, show))
		;//keep reading
	if (show)
		cout << "<语句列>\n";
	return 1;
}
//<步长>::= <无符号整数> 
int Syn_STEP(vector<Token>::iterator& iter, bool show = 0) {
	int ans = Syn_TK(iter, Token::INTCON, show);
	if (ans && show)
		cout << "<步长>\n";
	return ans;
}

/*<条件>   ::=  <表达式><关系运算符><表达式>//整型表达式之间才能进行关系运算

       ｜<表达式>   //表达式为整型，其值为0条件为假，值不为0时条件为真*/
int Syn_CONDITION(vector<Token>::iterator& iter, bool show = 0) {
	auto iter2 = iter;
	if (!Syn_EXP(iter2))
		return 0;
	Syn_EXP(iter, show);
	if (Syn_CMP(iter2)) {
		if (Syn_EXP(iter2)) {
			Syn_CMP(iter, show);
			Syn_EXP(iter, show);
		}
	}
	if (show)
		cout << "<条件>\n";
	return 1;
}
//<条件语句> ::= if '('<条件>')'<语句>［else<语句>］
int Syn_IF_STATE(vector<Token>::iterator& iter, bool show = 0) {
	auto iter2 = iter;
	if (!Syn_TK(iter2, Token::IFTK))
		return 0;
	if (!Syn_TK(iter2, Token::LPARENT))
		return 0;
	if (!Syn_CONDITION(iter2))
		return 0;
	if (!Syn_TK(iter2, Token::RPARENT))
		return 0;
	if (!Syn_STATEMENT(iter2, 0))
		return 0;
	Syn_TK(iter, Token::IFTK, show);
	Syn_TK(iter, Token::LPARENT, show);
	Syn_CONDITION(iter, show);
	Syn_TK(iter, Token::RPARENT, show);
	Syn_STATEMENT(iter, show);
	if (Syn_TK(iter2, Token::ELSETK)) {
		if (Syn_STATEMENT(iter2, 0)) {
			Syn_TK(iter, Token::ELSETK, show);
			Syn_STATEMENT(iter, show);
		}
	}
	if (show)
		cout << "<条件语句>\n";
	return 1;
}
//<赋值语句>  ::=  <标识符>＝<表达式>|<标识符>'['<表达式>']'=<表达式>
int Syn_ASSIGN_STATE(vector<Token>::iterator& iter, bool show = 0) {
	auto iter2 = iter;
	bool flag = 0;
	if (!Syn_IDENFR(iter2))
		return 0;
	if (Syn_TK(iter2, Token::LBRACK)) {
		if ((Syn_EXP(iter2))) {
			if (Syn_TK(iter2, Token::RBRACK)) {
				flag = 1;
			}
		}
	}
	if (!Syn_TK(iter2, Token::ASSIGN))
		return 0;
	if (!Syn_EXP(iter2))
		return 0;
	Syn_IDENFR(iter, show);
	if (flag) {
		Syn_TK(iter, Token::LBRACK, show);
		Syn_EXP(iter, show);
		Syn_TK(iter, Token::RBRACK, show);
	}
	Syn_TK(iter, Token::ASSIGN, show);
	Syn_EXP(iter, show);
	if (show)
		cout << "<赋值语句>\n";
	return 1;
}

//<循环语句>  ::=  while '('<条件>')'<语句>| do<语句>while '('<条件>')' |for'('<标识符>＝<表达式>;<条件>;<标识符>＝<标识符>(+|-)<步长>')'<语句>
int Syn_LOOP_STATE(vector<Token>::iterator& iter, bool show = 0) {
	auto iter2 = iter;
	bool flag = 0;
	if (Syn_TK(iter2, Token::WHILETK)) {
		if (!Syn_TK(iter2, Token::LPARENT))
			return 0;
		if (!Syn_CONDITION(iter2))
			return 0;
		if (!Syn_TK(iter2, Token::RPARENT))
			return 0;
		if (!Syn_STATEMENT(iter2,0))
			return 0;
		Syn_TK(iter, Token::WHILETK,show);
		Syn_TK(iter, Token::LPARENT,show);
		Syn_CONDITION(iter,show);
		Syn_TK(iter, Token::RPARENT,show);
		Syn_STATEMENT(iter,show);
		flag = 1;
	}
	else if (Syn_TK(iter2,Token::DOTK)) {
		if (!Syn_STATEMENT(iter2,0))
			return 0;
		if (!Syn_TK(iter2, Token::WHILETK))
			return 0;
		if (!Syn_TK(iter2, Token::LPARENT))
			return 0;
		if (!Syn_CONDITION(iter2))
			return 0;
		if (!Syn_TK(iter2, Token::RPARENT))
			return 0;
		Syn_TK(iter, Token::DOTK, show);
		Syn_STATEMENT(iter, show);
		Syn_TK(iter, Token::WHILETK, show);
		Syn_TK(iter, Token::LPARENT, show);
		Syn_CONDITION(iter, show);
		Syn_TK(iter, Token::RPARENT, show);
		flag = 1;
	}
	else if (Syn_TK(iter2, Token::FORTK)) {
		if (!Syn_TK(iter2, Token::LPARENT))
			return 0;
		if (!Syn_IDENFR(iter2))
			return 0;
		if (!Syn_TK(iter2, Token::ASSIGN))
			return 0;
		if (!Syn_EXP(iter2))
			return 0;
		if (!Syn_TK(iter2, Token::SEMICN))
			return 0;
		if (!Syn_CONDITION(iter2))
			return 0;
		if (!Syn_TK(iter2, Token::SEMICN))
			return 0;
		if (!Syn_IDENFR(iter2))
			return 0;
		if (!Syn_TK(iter2, Token::ASSIGN))
			return 0;
		if (!Syn_IDENFR(iter2))
			return 0;
		if (!Syn_PLUS(iter2))
			return 0;
		if (!Syn_STEP(iter2))
			return 0;
		if (!Syn_TK(iter2, Token::RPARENT))
			return 0;
		if (!Syn_STATEMENT(iter2,0))
			return 0;
		Syn_TK(iter, Token::FORTK,show);
		Syn_TK(iter, Token::LPARENT, show);
		Syn_IDENFR(iter, show);
		Syn_TK(iter, Token::ASSIGN, show);
		Syn_EXP(iter, show);
		Syn_TK(iter, Token::SEMICN, show);
		Syn_CONDITION(iter, show);
		Syn_TK(iter, Token::SEMICN, show);
		Syn_IDENFR(iter, show);
		Syn_TK(iter, Token::ASSIGN, show);
		Syn_IDENFR(iter, show);
		Syn_PLUS(iter, show);
		Syn_STEP(iter, show);
		Syn_TK(iter, Token::RPARENT, show);
		Syn_STATEMENT(iter, show);
		flag = 1;
	}
	if (flag && show)
		cout << "<循环语句>\n";
	return flag;
}
//'{'<语句列>'}'
inline int Syn_LBRACE_STATELIST_RBRACE(vector<Token>::iterator& iter, bool show = 0) {
	auto iter2 = iter;
	if (!Syn_TK(iter2, Token::LBRACE))
		return 0;
	if (!Syn_STATE_LIST(iter2))
		return 0;
	if (!Syn_TK(iter2, Token::RBRACE))
		return 0;
	Syn_TK(iter, Token::LBRACE, show);
	Syn_STATE_LIST(iter, show);
	Syn_TK(iter, Token::RBRACE, show);
	return 1;
}
/*<语句>   ::= <条件语句>｜<循环语句>| '{'<语句列>'}'| <有返回值函数调用语句>; 
                           |<无返回值函数调用语句>;｜<赋值语句>;｜<读语句>;｜<写语句>;｜<空>;|<返回语句>;*/
int Syn_STATEMENT(vector<Token>::iterator& iter, bool show = 0) {
	auto iter2 = iter;
	bool flag = 0;
	if (Syn_TK(iter, Token::SEMICN, show)) {
		flag = 1;
	}
	else if (Syn_IF_STATE(iter, show)) {
		flag = 1;
	}
	else if (Syn_LOOP_STATE(iter, show)) {
		flag = 1;
	}
	else if (Syn_LBRACE_STATELIST_RBRACE(iter, show)) {
		flag = 1;
	}
	else if(Syn_RE_FUNC_CALL(iter2)){
		if (!Syn_TK(iter2, Token::SEMICN))
			return 0;
		Syn_RE_FUNC_CALL(iter, show);
		Syn_TK(iter, Token::SEMICN, show);
		flag = 1;
	}
	else if (Syn_VOID_FUNC_CALL(iter2)) {
		if (!Syn_TK(iter2, Token::SEMICN))
			return 0;
		Syn_VOID_FUNC_CALL(iter, show);
		Syn_TK(iter, Token::SEMICN, show);
		flag = 1;
	}
	else if (Syn_ASSIGN_STATE(iter2)) {
		if (!Syn_TK(iter2, Token::SEMICN))
			return 0;
		Syn_ASSIGN_STATE(iter, show);
		Syn_TK(iter, Token::SEMICN, show);
		flag = 1;
	}
	else if (Syn_PRINTF_STATE(iter2)) {
		if (!Syn_TK(iter2, Token::SEMICN))
			return 0;
		Syn_PRINTF_STATE(iter, show);
		Syn_TK(iter, Token::SEMICN, show);
		flag = 1;
	}
	else if (Syn_SCANF_STATE(iter2)) {
		if (!Syn_TK(iter2, Token::SEMICN))
			return 0;
		Syn_SCANF_STATE(iter, show);
		Syn_TK(iter, Token::SEMICN, show);
		flag = 1;
	}
	else if (Syn_RETURN_STATE(iter2)) {
		if (!Syn_TK(iter2, Token::SEMICN))
			return 0;
		Syn_RETURN_STATE(iter, show);
		Syn_TK(iter, Token::SEMICN, show);
		flag = 1;
	}
	if (flag && show)
		cout << "<语句>\n";
	return flag;
}

//<程序>   ::= ［<常量说明>］［<变量说明>］{<有返回值函数定义>|<无返回值函数定义>}<主函数>
int Syn_PROGRAM(vector<Token>::iterator& iter, bool show = 0) {
	auto iter2 = iter;
	bool hasCONST = Syn_CONST_DEC(iter2);
	bool hasVAR = Syn_VAR_DEC(iter2);
	while (Syn_RE_FUNC_DEC(iter2) || Syn_VOID_FUNC_DEC(iter2))
		;//keep reading
	if (!Syn_MAIN_FUNC(iter2))
		return 0;
	
	if (hasCONST)
		Syn_CONST_DEC(iter, show);
	if (hasVAR)
		Syn_VAR_DEC(iter, show);
	while (Syn_RE_FUNC_DEC(iter,show) || Syn_VOID_FUNC_DEC(iter,show))
		;//keep reading
	Syn_MAIN_FUNC(iter,show);
	cout << "<程序>\n";
	return 1;
}
int SynMain(vector<Token> vector)
{
	
	Token token;

	for (int i = 0; i < 10; i++)
		vector.push_back(token);
	tokenVectorPointer = &vector;
	auto iter = vector.begin();

	Syn_PROGRAM(iter, 1);
	return 0;
}
